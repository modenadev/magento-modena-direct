<?php

namespace Webexpert\Direct\Controller\Request;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;


class SaveOption extends \Magento\Framework\App\Action\Action {

  protected $checkoutSession;

  protected $logger;

  protected $request;

  protected $messageManager;


  public function __construct(
    Context                                     $context,
    \Magento\Framework\App\Request\Http         $request,
    \Magento\Checkout\Model\Session             $checkoutSession,
    \Webexpert\Direct\Logger\Logger             $logger,
    \Magento\Framework\Message\ManagerInterface $messageManager
  ) {
    parent::__construct($context);
    $this->request = $request;
    $this->checkoutSession = $checkoutSession;
    $this->logger = $logger;
    $this->messageManager = $messageManager;

  }

  public function execute() {
    try {
      $quote = $this->checkoutSession->getQuote();
      if($quote){
        $quote->getPayment()->unsAdditionalInformation();
        $quote->getPayment()->setAdditionalInformation(
          'modena_option_code', $this->request->getParam('option_code')
        );
        $quote->getPayment()->setAdditionalInformation(
          'modena_option_label', str_replace("%", " ", $this->request->getParam('option_label'))
        );
        $quote->save();
      }
    } catch (\Exception $e) {
      $this->messageManager->addErrorMessage(__('Unable to save quote'));
    }
  }

}
