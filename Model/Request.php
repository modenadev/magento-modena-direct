<?php

namespace Webexpert\Direct\Model;

use Laminas\Http\Request as LaminasRequest;
use Magento\Sales\Api\Data\OrderInterface;
use Webexpert\Direct\Model\Adminhtml\Source\Mode;

class Request {

  public $scopeConfig;

  public $authToken;

  public $logger;

  public $url;

  public $mode;

  public $curlClient;

  public function __construct(
    \Magento\Framework\HTTP\LaminasClientFactory       $httpClientFactory,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Framework\UrlInterface                    $url,
    \Webexpert\Direct\Logger\Logger                    $logger,
    \Webexpert\Direct\Model\Adminhtml\Source\Mode      $mode,
    \Magento\Framework\HTTP\Client\Curl                $curl

  ) {
    $this->scopeConfig = $scopeConfig;
    $this->url = $url;
    $this->logger = $logger;
    $this->mode = $mode;
    $this->curlClient = $curl;
  }

  public function getClient() {
    $client = new \Laminas\Http\Client();
    $client->setMethod(LaminasRequest::METHOD_POST);
    $client->setHeaders(['Content-Type' => 'application/x-www-form-urlencoded']);
    return $client;
  }

  public function authenticate() {
    $data = [
      'grant_type' => 'client_credentials',
      'scope' => Mode::SCOPE,
    ];
    $client = $this->getClient();
    $client->setAuth($this->mode->getStoreId(), $this->mode->getSecret(), 'basic');
    $client->setUri(sprintf('%s/oauth2/token', $this->mode->getDirectUrl()));
    $client->setParameterPost($data);

    $response = $client->send();

    if ($response->isServerError() || $response->isClientError()) {
      $this->logger->info($response);
      throw new \Exception('Authentication with api failed');
    }

    $responseData = json_decode($response->getBody(), TRUE);
    $this->authToken = $responseData['access_token'];

    return $this;
  }

  public function initPurchase(OrderInterface $order) {
    $this->authenticate();
    $send = $this->setSend($order);
    try {
      $curl = $this->setCurl();
      $curl->post(sprintf('%s/%s', $this->mode->getDirectRedirect(), Mode::PATH), json_encode($send));
      if (array_key_exists('location', $curl->getHeaders())) {
        //TODO: figure out how to get this more elegantly
        $lines = explode("\n", $curl->getBody());
        $lines = array_slice($lines, -1);
        $result = json_decode(implode("\n", $lines), TRUE);
        $result['location'] = $curl->getHeaders()['location'];
        return $result;
      }
      else {
        $this->logger->info('Error when creating order: ' . $order->getIncrementId());
        throw new \Exception('Error when creating order: ' . $order->getIncrementId());
      }

    } catch (\Exception $e) {
      $this->logger->info($e->getMessage());
      throw $e;
    }
  }

  private function setSend($order) {
    if (!array_key_exists('modena_option_code', $order->getPayment()
      ->getAdditionalInformation())) {
      return FALSE;
    }
    $selectedOption = $order->getPayment()
      ->getAdditionalInformation()['modena_option_code'];

    $send = [
      "orderId" => $order->getIncrementId(),
      "selectedOption" => $selectedOption,
      "totalAmount" => $order->getGrandTotal(),
      "currency" => $order->getOrderCurrencyCode(),
      'orderItems' => [],
      'customer' =>
        [
          "firstName" => $order->getCustomerFirstName(),
          "lastName" => $order->getCustomerLastName(),
          "phoneNumber" => $order->getShippingAddress()->getTelephone(),
          "email" => $order->getCustomerEmail(),

        ],
      "timestamp" => str_replace(" ", "T", $order->getCreatedAt()) . 'Z',
      "returnUrl" => $this->url->getUrl('direct/response/success'),
      "cancelUrl" => $this->url->getUrl('direct/response/cancel'),
      "callbackUrl" => $this->url->getUrl('direct/response/callback'),

    ];
    $items = $order->getAllVisibleItems();
    foreach ($items as $item) {
      $send['orderItems'][] = [
        'description' => $item->getName(),
        'amount' => (string) $item->getRowTotalInclTax(),
        'currency' => $order->getOrderCurrencyCode(),
        'quantity' => round($item->getQtyOrdered()),
      ];
    }
    $send['orderItems'][] = [
      'description' => $order->getShippingDescription(),
      'amount' => $order->getShippingInclTax(),
      'currency' => $order->getOrderCurrencyCode(),
      'quantity' => '1',
    ];
    if($order->getDiscountAmount() != '0.0000'){
      $send['orderItems'][] = [
        'description' => 'Sooduskood',
        'amount' => $order->getDiscountAmount(),
        'currency' => $order->getOrderCurrencyCode(),
        'quantity' => '1',
      ];
    }
    return $send;
  }

  private function setCurl() {
    //using a seprate curl solution because the other one didnt seem to use some of the options we need
    $curl = $this->curlClient;
    $curl->setHeaders(
      [
        'Authorization' => 'Bearer ' . $this->authToken,
        'Content-Type' => 'application/json',
      ]
    );
    $curl->setOption(CURLOPT_SSL_VERIFYPEER, 0);
    $curl->setOption(CURLOPT_SSL_VERIFYHOST, 0);
    $curl->setOption(CURLOPT_URL, sprintf('%s/%s', $this->mode->getDirectRedirect(), Mode::PATH));
    $curl->setOption(CURLOPT_RETURNTRANSFER, 1);
    $curl->setOption(CURLOPT_HEADER, 1);
    $curl->setOption(CURLOPT_POST, 1);
    $curl->setOption(CURLOPT_FOLLOWLOCATION, FALSE);
    return $curl;

  }

  public function checkPurchase(OrderInterface $order) {

    $this->authenticate();
    $purchaseId = $order->getPayment()->getData()['direct_id'];
    if (!$purchaseId) {
      return FALSE;
    }
    try {
      $client = $this->getClient();
      $client->setUri(
        sprintf('%s/direct/api/partner-direct-payments/%s/status', $this->mode->getDirectRedirect(), $purchaseId)
      );
      $client->setHeaders(['Authorization' => sprintf('Bearer %s', $this->authToken)]);
        $client->setMethod(LaminasRequest::METHOD_GET);
      $response = $client->send();
      if ($response->isServerError() || $response->isClientError()) {
        throw new \Exception('Error when checking pending order status');
      }
      if ($response) {
        return json_decode($response->getBody(), TRUE)['status'];
      }
      else {
        return FALSE;
      }
    } catch (\Exception $e) {

      $this->logger->info($e->getMessage());
      throw $e;
    }

    return $responseData;
  }

  public function banksIcons() {

    $this->authenticate();
    $client = $this->getClient();
    $client->setUri(
      sprintf('%s/direct/api/payment-options', $this->mode->getDirectRedirect())
    );
    $client->setHeaders(['Authorization' => sprintf('Bearer %s', $this->authToken)]);
    try {
      $client->setMethod(LaminasRequest::METHOD_GET);
      $response = $client->send();
      if ($response->isServerError() || $response->isClientError()) {
        throw new \Exception('Error when checking Icons');
      }
      $responseData = json_decode($response->getBody());
    } catch (\Exception $e) {

      $this->logger->info($e->getMessage());
      return FALSE;

    }
    return $responseData;
  }


}
