<?php

namespace Webexpert\Direct\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Store\Model\ScopeInterface;

class Mode implements ArrayInterface {

  const SCOPE = 'directpayment';

  const PATH = 'direct/api/partner-direct-payments/payment-order';

  public $scopeConfig;

  public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
  ) {
    $this->scopeConfig = $scopeConfig;
  }

  public function toOptionArray() {
    return [
      [
        'value' => 'test',
        'label' => 'Test',
      ],
      [
        'value' => 'production',
        'label' => 'Production',
      ],
    ];
  }

  public function getDirectUrl() {
    if ($this->scopeConfig->getValue('payment/direct/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/direct/production_payment_url', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/direct/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/direct/test_payment_url', ScopeInterface::SCOPE_STORE);
      }
    }
    return NULL;
  }

  public function getDirectRedirect() {
    if ($this->scopeConfig->getValue('payment/direct/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/direct/production_redirect_url', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/direct/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/direct/test_redirect_url', ScopeInterface::SCOPE_STORE);

      }
    }
    return NULL;
  }
  public function getStoreId() {

    if ($this->scopeConfig->getValue('payment/direct/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/direct/store_id', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/direct/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/direct/test_store_id', ScopeInterface::SCOPE_STORE);

      }
    }
    return NULL;
  }
  public function getSecret() {

    if ($this->scopeConfig->getValue('payment/direct/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/direct/secret_key', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/direct/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/direct/test_secret_key', ScopeInterface::SCOPE_STORE);
      }
    }
    return NULL;
  }
}

