<?php

namespace Webexpert\Direct\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;

class OrderCheck {

  protected $transactionFactory;

  protected $orderInterface;

  protected $orderSender;

  protected $checkoutSession;

  protected $orderFactory;

  protected $quoteRepository;

  protected $invoiceSender;

  protected $_resource;

  protected $orderCollectionFactory;

  protected $configProvider;

  public function __construct(
    \Magento\Framework\DB\TransactionFactory                   $transactionFactory,
    \Magento\Sales\Api\Data\OrderInterface                     $orderInterface,
    \Magento\Sales\Model\Order\Email\Sender\OrderSender        $orderSender,
    \Magento\Checkout\Model\Session                            $checkoutSession,
    \Magento\Sales\Model\OrderFactory                          $orderFactory,
    \Magento\Quote\Model\QuoteRepository                       $quoteRepository,
    InvoiceSender                                              $invoiceSender,
    \Magento\Framework\App\ResourceConnection                  $resource,
    \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
    \Webexpert\Direct\Model\ConfigProvider                     $configProvider
  ) {
    $this->transactionFactory = $transactionFactory;
    $this->orderInterface = $orderInterface;
    $this->orderSender = $orderSender;
    $this->checkoutSession = $checkoutSession;
    $this->orderFactory = $orderFactory;
    $this->quoteRepository = $quoteRepository;
    $this->invoiceSender = $invoiceSender;
    $this->_resource = $resource;
    $this->orderCollectionFactory = $orderCollectionFactory;
    $this->configProvider = $configProvider;
  }

  public function processOrder($order) {
    if ($order->hasInvoices() || in_array($order->getState(),
        [Order::STATE_CANCELED, Order::STATE_COMPLETE, Order::STATE_CLOSED])
    ) {
      throw new LocalizedException(__('Order could not be processed.'));
    }

    $this->sendOrderEmail($order);

    if ($order->canInvoice()) {
      $order->setState(Order::STATE_PROCESSING);
      $this->createInvoice($order);

      try {
        $invoice = $order->getInvoiceCollection()->getFirstItem();
        $this->invoiceSender->send($invoice);
      } catch (\Exception $e) {
        throw new LocalizedException(__('Invoice email could not be sent.'));
      }
    }

    $this->checkoutSession->getQuote()
      ->setIsActive(FALSE)
      ->save();

    return TRUE;
  }

  public function sendOrderEmail($order) {
    if (!$order->getEmailSent()) {
      $this->orderSender->send($order);
    }
  }

  public function createInvoice($order) {
    $invoice = $order->prepareInvoice();

    if (!$invoice || !$invoice->getTotalQty()) {
      throw new LocalizedException(__('Invoice could not be created.'));
    }

    $invoice->register();
    $invoice->capture();

    $this->transactionFactory->create()
      ->addObject($order->getPayment())
      ->addObject($invoice)
      ->addObject($order)
      ->save();

    $quote = $this->quoteRepository->get($order->getQuoteId());
    $quote->setReservedOrderId($order->getRealOrderId())->setIsActive(FALSE);
    $this->quoteRepository->save($quote);
  }

  public function cancelOrder($order) {
    if ($order && $order->getId()) {
      $order->cancel()->save();
    }
  }

  public function saveOrderPurchaseId($orderId, $purchaseId) {
    $connection = $this->_resource->getConnection();
    $where = ['parent_id = ?' => $orderId];
    $bind = [
      'direct_id' => $purchaseId,
    ];
    $this->_resource->getConnection()
      ->update($this->_resource->getTableName('sales_order_payment'), $bind, $where);
  }

  public function getOrder($incrementId, $paymentId) {

    $collection = $this->orderCollectionFactory->create();
    $collection->getSelect()->join(
      ['p' => $collection->getTable('sales_order_payment')],
      'p.parent_id = main_table.entity_id',
      'p.method'
    )
      ->where('p.method = ?', ConfigProvider::DIRECT_CODE)
      ->where('main_table.increment_id = ?', $incrementId)
      ->where('p.direct_id = ?', $paymentId);
    foreach ($collection as $order) {
      return $order;
    }

    return FALSE;
  }


}
