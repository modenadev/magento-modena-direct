<?php

namespace Webexpert\Direct\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class ConfigProvider implements ConfigProviderInterface {

  const DIRECT_CODE = 'direct';

  protected $urlInterface;

  private $storeManager;

  private $scopeConfig;

  private $checkoutSession;

  private $currencyCode;

  private $priceHelper;

  private $quoteFactory;

  protected $request;

  public function __construct(
    \Magento\Framework\UrlInterface        $urlInterface,
    StoreManagerInterface                  $storeManager,
    ScopeConfigInterface                   $scopeConfig,
    \Magento\Checkout\Model\Session        $checkoutSession,
    CurrencyFactory                        $currencyFactory,
    \Magento\Framework\Pricing\Helper\Data $priceHelper,
    \Magento\Quote\Model\QuoteFactory      $quoteFactory,
    \Webexpert\Direct\Model\Request        $request
  ) {
    $this->urlInterface = $urlInterface;
    $this->storeManager = $storeManager;
    $this->scopeConfig = $scopeConfig;
    $this->checkoutSession = $checkoutSession;
    $this->currencyCode = $currencyFactory->create();
    $this->priceHelper = $priceHelper;
    $this->quoteFactory = $quoteFactory;
    $this->request = $request;
  }

  public function getConfig() {

    return [
      'payment' => [
        self::DIRECT_CODE => [
          'redirect_url' => $this->urlInterface->getUrl('direct/request/redirect'),
          'code' => self::DIRECT_CODE,
          'icons' => $this->getIcons(),
          'oneliner'   => $this->scopeConfig->getValue('payment/direct/oneliner', ScopeInterface::SCOPE_STORE),
          'description' => $this->scopeConfig->getValue('payment/direct/description', ScopeInterface::SCOPE_STORE),
        ],
      ],
    ];
  }

  private function getLogoUrl() {
    $image = $this->scopeConfig->getValue('payment/direct/image_remote', ScopeInterface::SCOPE_STORE);

    if ($image) {
      return $image;
    }
    else {
      return FALSE;
    }
  }

  private function getSelected() {
    if ($this->checkoutSession->getQuote()) {
      $quote = $this->checkoutSession->getQuote();
      if (array_key_exists('modena_option_label', $quote->getPayment()
        ->getAdditionalInformation())) {
        return $quote->getPayment()
          ->getAdditionalInformation()['modena_option_code'];
      }
    }
    return '';
  }

  private function getIcons() {
    $quoteId = '';
    $count = 1;
    if ($this->checkoutSession->getQuote()) {
      $quoteId = $this->checkoutSession->getQuote()->get('id')['entity_id'];
    }
    $result = $this->request->banksIcons();
    if (!$result) {
      return [];
    }
    $selected = $this->getSelected();
    foreach ($result as $key => $item) {
      $result[$key]->quote = $quoteId;
      $result[$key]->domainUrl = $this->storeManager->getStore()->getBaseUrl();
      if ($selected == $item->code) {
        $result[$key]->selected = 1;
      }
      else {
        $result[$key]->selected = 0;
      }
      if ($item->code == 'HABAEE2X' && !$selected){
        $result[$key]->selected = 1;
      }
    }
    return $result;
  }

}
