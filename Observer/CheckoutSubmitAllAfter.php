<?php

namespace Webexpert\Direct\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webexpert\Direct\Model\ConfigProvider;
use Magento\Store\Model\ScopeInterface;

class CheckoutSubmitAllAfter implements ObserverInterface {

    private $checkoutSession;
    private $scopeConfig;

    public function __construct(
        \Magento\Checkout\Model\Session\Proxy $checkoutSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
    }


  public function execute(\Magento\Framework\Event\Observer $observer) {
    $quote = $observer->getQuote();
    if ($quote->getPayment()
        ->getMethodInstance()
        ->getCode() == ConfigProvider::DIRECT_CODE &&  $this->scopeConfig->getValue('payment/direct/restore_quote', ScopeInterface::SCOPE_STORE)) {
      $quote->setReservedOrderId(NULL)
        ->setIsActive(TRUE)
        ->save();
    }
    return $this;
  }
}

