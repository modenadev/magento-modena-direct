<?php

namespace Webexpert\Direct\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webexpert\Direct\Model\ConfigProvider;


class OrderEmailCancel implements ObserverInterface {

  public function execute(\Magento\Framework\Event\Observer $observer) {
    $quote = $observer->getEvent()->getQuote();
    /** @var  \Magento\Sales\Model\Order $order */
    $order = $observer->getEvent()->getOrder();

    if ($this->hasPaymentMethod($quote)) {
      $order->setCanSendNewEmailFlag(FALSE);
    }
    else {
      $order->getPayment()->unsAdditionalInformation(
        'modena_option_code'
      );
      $order->getPayment()->unsAdditionalInformation(
        'modena_option_label'
      );
    }

    return $this;
  }

  protected function hasPaymentMethod($quote) {
    return $quote->getPayment()
        ->getMethodInstance()
        ->getCode() == ConfigProvider::DIRECT_CODE;
  }

}
