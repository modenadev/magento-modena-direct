<?php

namespace webexpert\Direct\Block;

class AdminOption extends \Magento\Sales\Block\Adminhtml\Order\View {

  protected $_bankTransfer;

  protected $configProvider;

  public function __construct(
    \Magento\Backend\Block\Widget\Context  $context,
    \Magento\Framework\Registry            $registry,
    \Magento\Sales\Model\Config            $salesConfig,
    \Magento\Sales\Helper\Reorder          $reorderHelper,
    \Webexpert\Direct\Model\ConfigProvider $configProvider,
    array                                  $data = []
  ) {

    $this->_coreRegistry = $registry;
    $this->_salesConfig = $salesConfig;
    $this->configProvider = $configProvider;
    parent::__construct($context, $registry, $salesConfig, $reorderHelper, $data);
  }

}
