define([
  "uiComponent",
  "Magento_Checkout/js/model/payment/renderer-list",
], function (Component, rendererList) {
  "use strict";
  rendererList.push({
    type: "direct",
    component: "Webexpert_Direct/js/view/payment/method-renderer/direct-method",
  });
  return Component.extend({});
});
