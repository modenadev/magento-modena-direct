/**
 * Copyright ©  Webexpert Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
define([
  "jquery",
  "Magento_Checkout/js/view/payment/default",
  "Magento_Checkout/js/action/place-order",
  "Webexpert_Direct/js/action/set-payment-method",
  "mage/translate",
], function (
  $,
  Component,
  placeOrderAction,
  setPaymentMethod,
  additionalValidators,
  $translate
) {
  "use strict";
  return Component.extend({
    redirectAfterPlaceOrder: false,
    displayLogo: window.checkoutConfig.payment.direct.display_logo,
    defaults: {
      template: "Webexpert_Direct/payment/direct",
    },
    getCode: function () {
      return window.checkoutConfig.payment.direct.code;
    },
    getBanksIcons: function () {
      return window.checkoutConfig.payment.direct.icons;
    },
    getOneliner: function () {
      return window.checkoutConfig.payment.direct.oneliner;
    },
    getDescription: function () {
      return window.checkoutConfig.payment.direct.description;
    },
    getData: function () {
      return {
        method: this.item.method,
        additional_data: {},
      };
    },
    afterPlaceOrder: function () {
      setPaymentMethod();
    },
  });
});

function applyBankLinkOption(code, label, quoteId, domainUrl) {
  if (code) {
    var urlDomain;
    urlDomain =
      domainUrl +
      "direct/request/saveOption?option_code=" +
      code +
      "&option_label=" +
      label.replace(/\s/g, "%");
    jQuery.ajax(urlDomain, {}, function () {});
  }
  return true;
}
